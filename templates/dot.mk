# ----------------------------------------------------------------------------
# Generate GraphViz diagrams
# ----------------------------------------------------------------------------
# Parameter: `FILENAMESTART=<file-name-start>`
# ----------------------------------------------------------------------------

DOTS			:=	$(wildcard $(FILENAMESTART).dot) \
					$(wildcard $(FILENAMESTART)-*.dot)
DOT_PDFS		:=	$(DOTS:%.dot=%.pdf)

# ----------------------------------------------------------------------------

# Cleaner
CLEAN				= rm -f

# ----------------------------------------------------------------------------

# Genarate PDFs from DOTs
%.pdf: %.dot
	@echo $@
	@dot -Tpdf -o $@ $(@:.pdf=.dot)

# ----------------------------------------------------------------------------

generate-diagrams: $(DOT_PDFS)

clean:
	@$(CLEAN) $(DOT_PDFS)

# ----------------------------------------------------------------------------

.PHONY: all clean generate-diagrams

.DEFAULT_GOAL := all

all: generate-diagrams
