# ----------------------------------------------------------------------------
# Generate PlantUML diagrams
# ----------------------------------------------------------------------------
# Parameter: `FILENAMESTART=<file-name-start>`
# ----------------------------------------------------------------------------

PUMLS			:=	$(wildcard $(FILENAMESTART).puml) \
					$(wildcard $(FILENAMESTART)-*.puml)
DEP_PUMLS		:=	$(wildcard $(FILENAMESTART)/*.puml)
# PUMLS			:=	$(filter-out class/*.puml, $(PUMLS))
PUML_TEXES		:=	$(PUMLS:%.puml=../images/%.latex)
PUML_SVGS		:=	$(PUMLS:%.puml=../images/%.svg)

PLANTUML		:= plantuml
# PLANTUML		:= docker run --rm -v $(shell pwd):/data dstockhammer/plantuml:latest

# ----------------------------------------------------------------------------

# Cleaner
CLEAN				= rm -f

# ----------------------------------------------------------------------------

# Genarate latex-files from PlantUML-files
../images/%.latex: %.puml ../templates/format.puml $(DEP_PUMLS)
	@echo $@
	@$(PLANTUML) -tlatex:nopreamble -o "../images" "./$<"

# Genarate svg-files from PlantUML-files
../images/%.svg: %.puml ../templates/format.puml $(DEP_PUMLS)
	@echo $@
	$(PLANTUML) -tsvg -o "../images" "./$<"

# ----------------------------------------------------------------------------

generate-diagrams: $(PUML_TEXES) $(PUML_SVGS)

clean:
	@$(CLEAN) $(PUML_TEXES) $(PUML_SVGS)

# ----------------------------------------------------------------------------

.PHONY: all clean generate-diagrams

.DEFAULT_GOAL := all

all: generate-diagrams
