# ----------------------------------------------------------------------------
# Generate a PDF document from a tex file
# ----------------------------------------------------------------------------

generate-diagrams:
	@$(MAKE) --no-print-directory -C ../diagrams

build: $(MAIN_LATEX_FILENAME).tex
	@latexmk -pdf -interaction=nonstopmode \
		-pdflatex="lualatex -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape %O %S" \
		$(shell pwd)/$(MAIN_LATEX_FILENAME).tex

clean:
	@latexmk -c
	@rm -f $(MAIN_LATEX_FILENAME).bbl

# ----------------------------------------------------------------------------

.PHONY: all clean generate-diagrams build

.DEFAULT_GOAL := all

all: clean build
