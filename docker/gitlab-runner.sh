#!/bin/sh
set -e

docker-compose \
    -f ./docker/config/gitlab-runner.yml ^
    $@
