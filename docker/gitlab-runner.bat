@docker-compose ^
    --env-file .env ^
    -f ./docker/config/gitlab-runner.yml ^
    %*
